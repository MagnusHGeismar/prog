#include<stdio.h>
#include<math.h>
#include<stdlib.h>

/*argc = argument count, number of arguments passed to the program
argv = argument vector, the array of arguments*/


int main(int argc, char *argv[]){
 for (int i=1; i<argc;i++){
   double x = atof(argv[i]); /*atof converts a string to a double (i.e. the character 2 is converted to the actual number 2 stored as a double*/
   printf("%lg \t %lg\n",x,sin(x));
 }
}
