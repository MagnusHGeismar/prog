#include<stdio.h>
#include<math.h>

int main(){
  double x;
  while (scanf("%lg",&x) != EOF){printf("%lg \t %lg\n",x,cos(x));} /*while scanf does not return "EOF" (Input error), store the std. input of form double into x, and print x,cos(x) separated by a tabular, and change line.  */
  
  return 0;
}
