#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"
#include"assert.h"

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v); v=NULL; }

void nvector_set(nvector* v, int i, double value){ assert(0 <= i && i< v->size) ;(*v).data[i]=value; }

double nvector_get(nvector* v, int i){ assert(0 <= i && i< v->size); return (*v).data[i]; }

void nvector_add(nvector* u,nvector* v){
  if (u->size == v->size){
    for (int i=0;i < (*u).size;i++){(*u).data[i] = (*u).data[i]+(*v).data[i];}
  }
  else{printf("Vectors not of equal size\n");}
}

double nvector_dot_product(nvector* u, nvector* v){
  double result = 0;
  if((*u).size == (*v).size){
    for(int i=0;i<(*u).size;i++){result += (*u).data[i]*(*v).data[i];}
    return result;}
  else {return 0; printf("Vectors not of equal size\n");}
}

void nvector_print(char* s, nvector* v){
  printf("%s",s);
  printf("(");
  for(int i=0 ; i<(*v).size ; i++){printf("%g ",(*v).data[i]);}
  printf(")\n");
}

int nvector_equal(nvector* v, nvector* u){
  if(v->size == u->size){
    int k = 0;
    for (int i=0 ; i<(*v).size ; i++){
      if(v->data[i] == u->data[i]){k++;}
    }
    if(k == v->size){return 1;}
    else{return 0;}
  }
  else{return 0;}
}

void nvector_set_zero(nvector* v){
  for (int i=0; i< v->size;i++){
    (*v).data[i] = 0;
  }
}

void nvector_sub(nvector* u,nvector* v){
  if (u->size == v->size){
    for (int i=0;i < (*u).size;i++){(*u).data[i] = (*u).data[i]-(*v).data[i];}
  }
  else{printf("Vectors not of equal size\n");}
}

void nvector_scale(nvector* a, double x){
  for (int i=0; i< (*a).size; i++){
    (*a).data[i] *= x;
  }
}
