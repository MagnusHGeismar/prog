#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_eq(double x, const double y[], double dydt[], void *params){
  dydt[0] = 2/sqrt(M_PI)*exp(-pow(x,2.0));
  return GSL_SUCCESS;
}

int main(int argc, char *argv[]){
  double a = atof(argv[1]), b = atof(argv[2]), dx = atof(argv[3]);


  gsl_odeiv2_system system;
  system.function = diff_eq ;
  system.jacobian = NULL ;
  system.dimension = 1 ;
  system.params = NULL ;

  double startpoint = 0, y[1] = {0}, abserr = 1e-3, relerr = 1e-3;

/*since our initial stepsize is positive in the forloop, we need our initial conditions
to be given at y(a), not y(0), if a<0. The prepdriver moves downwards, and so generates
y(a-dx) and passes that as initial conditions for the actual driver.*/
  if( a<0 ){
    double stepini = -0.05;
    gsl_odeiv2_driver *prepdriver = gsl_odeiv2_driver_alloc_y_new(
      &system, gsl_odeiv2_step_rkf45, stepini, abserr, relerr
    );

    printf("%g \t",a-dx);
    int status = gsl_odeiv2_driver_apply(prepdriver,&startpoint,a-dx,y);
    if (status != GSL_SUCCESS){fprintf(stderr,"Error in integration, status: %i",status);}
    printf("%g\n",y[0]);

    gsl_odeiv2_driver_free(prepdriver);
  }

  double stepini = 0.05;
  gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(
    &system, gsl_odeiv2_step_rkf45, stepini, abserr, relerr
  );


  for (double x=a; x <= b; x+= dx){
    printf("%g \t",x);
    int status = gsl_odeiv2_driver_apply(driver,&startpoint,x,y);
    if (status != GSL_SUCCESS){fprintf(stderr,"Error in integration, status: %i",status);}
    printf("%g\n",y[0]);
  }

  gsl_odeiv2_driver_free(driver);
}
