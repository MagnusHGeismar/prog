#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double normsquare(double x,void * params){
  double a = *(double *) params;
  return exp(-a*pow(x,2.0));
}

double normsquare_int(double alpha){

  int maxstep = 100;

  gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(maxstep);


  double epsabs = 1e-6, epsrel = 1e-6;
  double result, abserr;

  gsl_function f;
  f.function = normsquare;
  f.params = (void *)&alpha;

  int status = gsl_integration_qagi(
    &f,epsabs,epsrel,maxstep,workspace,
    &result,&abserr
  );
  if(status != GSL_SUCCESS){
    return NAN;
  }
  else return result;

  gsl_integration_workspace_free(workspace);

}

double hamilton(double x,void * params){
  double a = *(double *) params;
  return (-pow(a*x,2.0)/2.0 + a/2.0 + pow(x,2)/2.0) * exp(-a*pow(x,2.0));
}

double hamilton_int(double alpha){

  int maxstep = 100;

  gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(maxstep);


  double epsabs = 1e-6, epsrel = 1e-6;
  double result, abserr;

  gsl_function f;
  f.function = hamilton;
  f.params = (void *)&alpha;

  int status = gsl_integration_qagi(
    &f,epsabs,epsrel,maxstep,workspace,
    &result,&abserr
  );
  if(status != GSL_SUCCESS){
    return NAN;
  }
  else return result;

  gsl_integration_workspace_free(workspace);

}
