#include<stdio.h>
#include<math.h>
#include<gsl/gsl_min.h>
#include<gsl/gsl_errno.h>

double integral1();
double hamilton_int(double alpha);
double normsquare_int(double alpha);

double energy(double alpha,void * params){
  return hamilton_int(alpha)/normsquare_int(alpha);
}

int main(){

  double result1 = integral1();
  printf("The result of the numerical calculation of the integral is %g\n",result1);
/*Setting up the energy as a function of alpha, as a gsl function*/
  gsl_function E;
  E.function = energy;
  E.params = NULL;
/*From here on I simply follow the instructions for using the
gsl minimisation routines, using the golden section algorithm.*/
  gsl_min_fminimizer * minimizer = gsl_min_fminimizer_alloc(gsl_min_fminimizer_goldensection);

  double alpha_min = 0.1, alpha_max = 10, alpha_guess = 5;
  int status = gsl_min_fminimizer_set(
    minimizer,&E,alpha_guess,alpha_min,alpha_max
  );

  int maxstep = 100, i=0;
  double epsabs = 1e-6, epsrel = 1e-6;
  do{
    i++ ;
    status = gsl_min_fminimizer_iterate(minimizer);

    alpha_min = gsl_min_fminimizer_x_lower(minimizer);
    alpha_max = gsl_min_fminimizer_x_upper(minimizer);
    alpha_guess = gsl_min_fminimizer_x_minimum(minimizer);

    status =
        gsl_min_test_interval(alpha_min,alpha_max,epsabs,epsrel);

    if(status == GSL_SUCCESS){
      printf("Energy minimum found at alpha = %g, E = %g\n",alpha_guess,energy(alpha_guess,NULL));
    }
  }
  while (status == GSL_CONTINUE && i<=maxstep);

  if(i == maxstep){printf("Maxstep reached\n");}

  gsl_min_fminimizer_free(minimizer);

  /*Creating data for plot*/

  FILE *data = fopen("plot.txt","w");
  if(data == NULL){printf("Error opening plot.txt\n");}

  for (double x=0.1; x<10; x+=0.05){
    fprintf(data,"%g\t%g\n",x,energy(x,NULL));
  }

  fclose(data);

  return 0;
}
