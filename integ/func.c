#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double integrand(double x,void * params){
  return log(x)/sqrt(x);
}

double integral1(){

  int maxstep = 100;

  gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(maxstep);

  double uplim = 1, lowlim = 0;
  double epsabs = 1e-6, epsrel = 1e-6;
  double result, abserr;

  gsl_function f;
  f.function = integrand;
  f.params = NULL;

  int status = gsl_integration_qag(
    &f,lowlim,uplim,epsabs,epsrel,maxstep,GSL_INTEG_GAUSS61,workspace,
    &result,&abserr
  );
  if(status != GSL_SUCCESS){
    return NAN;
  }
  else return result;

  gsl_integration_workspace_free(workspace);

}
