#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

/*This function is essentially the system of linked differential equations*/
int orbit(double x, const double y[], double dydt[], void *params){
  double eps = *(double *) params; /*ask Dmitri about this line*/
  dydt[0] = y[1];
  dydt[1] = 1 - y[0] + eps*pow(y[0],2);
  return GSL_SUCCESS;
}

int main(){
/*Here we create an object which has the full description of the problem.
The system of equations from above are thrown into the element .fucntion of this struct*/
  double eps,u0,du0dx;
  scanf("%lg %lg %lg",&eps,&u0,&du0dx); /*takes the parameter eps from the standard input*/

  gsl_odeiv2_system system;
  system.function = orbit ;
  system.jacobian = NULL ;
  system.dimension = 2 ;
  system.params = (void *) &eps ;

/*We now allocate a workspace for the driver to work in*/
  double stepini = 0.05, abserr = 1e-3, relerr = 1e-3;
  gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(
    &system, gsl_odeiv2_step_rkf45, stepini, abserr, relerr
  );

/*We are now ready to let the driver run. To acquire a plot we only apply
 the driver to a small segment at a time, and put a for-loop around it
 to repeat.*/
  double jump = 0.2, startpoint = 0;
  /*We now specify an extra value in our array y[], namely y(startpoint)*/
  double y[2]={u0,du0dx}; /*y is an array with 2 elements, of value 0 and 1*/
  for (double x=0; x <= 500.0; x+=jump){
    int report = gsl_odeiv2_driver_apply(driver,&startpoint,x,y);
    if (report != GSL_SUCCESS){fprintf(stderr,"Error in integration, report: %i",report);}
    printf("%g \t%g\n",x,y[0]);
  }


  gsl_odeiv2_driver_free(driver);
}
