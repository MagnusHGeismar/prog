#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

/*This function is essentially the system of linked differential equations*/
int diff_eq(double x, const double y[], double dydt[], void *params){
  dydt[0] = y[0]*(1.0-y[0]);
  return GSL_SUCCESS;
}

int main(){
/*Here we create an object which has the full description of the problem.
The system of equations from above are thrown into the element .fucntion of this struct*/
  gsl_odeiv2_system system;
  system.function = diff_eq ;
  system.jacobian = NULL ;
  system.dimension = 1 ;
  system.params = NULL ;

/*We now allocate a workspace for the driver to work in*/
  double stepini = 0.05, abserr = 1e-3, relerr = 1e-3;
  gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(
    &system, gsl_odeiv2_step_rkf45, stepini, abserr, relerr
  );

/*We are now ready to let the driver run. To ensure accuracy we only apply the driver to
a small segment at a time, and put a for-loop around it to repeat.*/
  double jump = 0.2, startpoint = 0;
  /*We now specify an extra value in our array y[], namely y(startpoint)*/
  double y[1]={0.5};
  for (double x=0; x <= 3.0; x+=jump){
    int report = gsl_odeiv2_driver_apply(driver,&startpoint,x,y);
    if (report != GSL_SUCCESS){fprintf(stderr,"Error in integration, report: %i",report);}
    double analytic = 1.0 / (1.0 + exp(-x));
    printf("%g \t%g \t%g\n",x,y[0],analytic);
  }


  gsl_odeiv2_driver_free(driver);
}
