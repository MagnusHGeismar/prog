#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<stdlib.h>

#define ALG gsl_multimin_fminimizer_nmsimplex2

struct data {int size; double *t, *y, *sigma;};

double func(double A,double T,double B,double t){
  return A*exp(-t/T)+B;
}

double master(const gsl_vector * x, void * params){

  double A = gsl_vector_get(x,0);
  double T = gsl_vector_get(x,1);
  double B = gsl_vector_get(x,2);

  struct data *v = (struct data *) params;

  double *t = v->t;
  double *y = v->y;
  double *sigma = v->sigma;
  int size = v->size;

  double squares = 0;
    for (int i= 0; i<size; i++){
      squares += pow(func(A,T,B,t[i]) - y[i],2)/pow(sigma[i],2);
    }
  return squares;
}

int main(){
  printf("\nLeast Squares fit:\n\n");

  const int dim = 3;

  double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
  double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
  double sigma[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
  int size = sizeof(t)/sizeof(t[0]);

  struct data v;
  v.size = size;
  v.t = t;
  v.y = y;
  v.sigma = sigma;

  gsl_multimin_function squares;
  squares.f = master;
  squares.n = dim;
  squares.params = (void*) &v;

  /*Setting initial data:*/
  gsl_vector *x = gsl_vector_alloc(3);
  gsl_vector * step = gsl_vector_alloc(3);

  /*guessing x,y close to the minimum*/
  gsl_vector_set(x,0,2);
  gsl_vector_set(x,1,2);
  gsl_vector_set(x,2,2);

  /*setting intial stepsizes for x and y:*/
  gsl_vector_set_all(step,0.1);

  gsl_multimin_fminimizer * minimizer = gsl_multimin_fminimizer_alloc(ALG,dim);
  gsl_multimin_fminimizer_set(minimizer,&squares,x,step);

  int iter = 0, status;
  double epsabs = 1e-3,check;
  do{
    iter++;

    status = gsl_multimin_fminimizer_iterate(minimizer);

    if(status!=0) {break; printf("error in iteration");}

    check = gsl_multimin_fminimizer_size(minimizer);
    status = gsl_multimin_test_size(check,epsabs);

    if(status == GSL_SUCCESS){
      double A = gsl_vector_get(minimizer->x,0);
      double T = gsl_vector_get(minimizer->x,1);
      double B = gsl_vector_get(minimizer->x,2);
      printf("Minimum found with accuracy %g\n \
       at (A,T,B) = (%g,%g,%g) after %i iterations\n \
      value = %g \n",epsabs,A,T,B,iter,master(minimizer->x, (void*) &v));
    }

  }while(status == GSL_CONTINUE && iter < 100);
  if (iter == 100) printf("max iterations reached.\n");

/*Setting up data for a plot of our resulting function:*/
if(status == GSL_SUCCESS){
  double A = gsl_vector_get(minimizer->x,0);
  double T = gsl_vector_get(minimizer->x,1);
  double B = gsl_vector_get(minimizer->x,2);

  double F[size];
  for (int j = 0; j<size; j++){
    F[j] = A*exp(-t[j]/T)+B;
  }


  FILE *plot = fopen("plot.txt","w");
  if(plot == NULL){printf("Error opening plot.txt\n");}

  for (int j = 0;j<size; j++){
    fprintf(plot,"%g\t%g\t%g\n",t[j],F[j],y[j]);
  }

  fclose(plot);
}
  gsl_multimin_fminimizer_free(minimizer);
  gsl_vector_free(x);
  gsl_vector_free(step);
}
