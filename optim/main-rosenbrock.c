#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<stdlib.h>

#define ALG gsl_multimin_fminimizer_nmsimplex2

double rosenbrock(double x, double y){
  return pow(1-x,2) + 100*pow(y-pow(x,2),2);
}

double masterf(const gsl_vector * v, void * params){
  double x = gsl_vector_get(v,0);
  double y = gsl_vector_get(v,1);
  return rosenbrock(x,y);
}

int main(){
  printf("\nMinimizing the Rosenbrock function:\n\n");

  const int dim = 2;

  gsl_multimin_function rbf;
  rbf.f = masterf;
  rbf.n = dim;
  rbf.params = NULL;

  /*Setting initial data:*/
  gsl_vector *v = gsl_vector_alloc(2);
  gsl_vector * step = gsl_vector_alloc(2);

  /*guessing x,y close to the minimum*/
  gsl_vector_set(v,0,3);
  gsl_vector_set(v,1,2);

  /*setting intial stepsizes for x and y:*/
  gsl_vector_set_all(step,0.1);

  gsl_multimin_fminimizer * minimizer = gsl_multimin_fminimizer_alloc(ALG,dim);
  gsl_multimin_fminimizer_set(minimizer,&rbf,v,step);

  int iter = 0, status;
  double epsabs = 1e-6,size;
  do{
    iter++;

    status = gsl_multimin_fminimizer_iterate(minimizer);

    if(status!=0) {break; printf("error in iteration");}

    size = gsl_multimin_fminimizer_size(minimizer);
    status = gsl_multimin_test_size(size,epsabs);

    if(status == GSL_SUCCESS){
      double x = gsl_vector_get(minimizer->x,0);
      double y = gsl_vector_get(minimizer->x,1);
      printf("Minimum found with accuracy %g at (%g,%g) after %i iterations\n \
      value = %g \n",epsabs,x,y,iter,rosenbrock(x,y));
    }

  }while(status == GSL_CONTINUE && iter < 100);
  if (iter == 100) printf("max iterations reached.\n");

  printf("global analytical minimum is at (1,1) with value %g\n",rosenbrock(1,1));

  gsl_multimin_fminimizer_free(minimizer);
  gsl_vector_free(v);
  gsl_vector_free(step);
}
