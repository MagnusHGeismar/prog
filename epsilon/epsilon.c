#include<stdio.h>
#include<float.h>

int main(){
  printf("\n\n for double \n\n");
  double x=1;
  while(1+x!=1){x/=2;} /*!= means "not equal to". x/=2 is equivalent to x= x/2. */
  x*=2; /*equivalent to x=x*2*/
  printf("epsilonwhile = %g\n",x);

  for(x=1;1+x!=1;x/=2){}
  x*=2;
  printf("epsilonfor = %g\n",x);

  x=1;
  do {x/=2;}
  while(x+1!=1);
  x*=2;
  printf("epsilondowhile = %g\n",x);
  printf("epsilon from float.h = %g\n",DBL_EPSILON);

  printf("\n\n for long double\n\n");
  
  long double e=1;
  while(1+e!=1){e/=2;} /*!= means "not equal to". x/=2 is equivalent to x= x/2. */
  e*=2; /*equivalent to x=x*2*/
  printf("epsilonwhile = %Lg\n",e);

  for(e=1;1+e!=1;e/=2){}
  e*=2;
  printf("epsilonfor = %Lg\n",e);

  e=1;
  do {e/=2;}
  while(e+1!=1);
  e*=2;
  printf("epsilondowhile = %Lg\n",e);
  printf("epsilon from float.h = %Lg\n",LDBL_EPSILON);

  printf("\n\n for float\n\n");

  float y=1;
  while(1+y!=1){y/=2;} /*!= means "not equal to". x/=2 is equivalent to x= x/2. */
  y*=2; /*equivalent to x=x*2*/
  printf("epsilonwhile = %g\n",y);

  for(y=1;1+y!=1;y/=2){}
  y*=2;
  printf("epsilonfor = %g\n",y);

  y=1;
  do {y/=2;}
  while(y+1!=1);
  y*=2;
  printf("epsilondowhile = %g\n",y);
  printf("epsilon from float.h = %g\n",FLT_EPSILON);


}
