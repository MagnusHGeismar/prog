

 for double 

epsilonwhile = 2.22045e-16
epsilonfor = 2.22045e-16
epsilondowhile = 2.22045e-16
epsilon from float.h = 2.22045e-16


 for long double

epsilonwhile = 1.0842e-19
epsilonfor = 1.0842e-19
epsilondowhile = 1.0842e-19
epsilon from float.h = 1.0842e-19


 for float

epsilonwhile = 1.19209e-07
epsilonfor = 1.19209e-07
epsilondowhile = 1.19209e-07
epsilon from float.h = 1.19209e-07
