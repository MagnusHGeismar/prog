#include "stdio.h"
#include "math.h"
#include "complex.h"

int main(){
  double complex sqrt = csqrt(-2.0);
  double complex ei = cexp(I);
  double complex eipi = cexp(I*M_PI);
  double complex ie = cpow(I,M_E);
  printf("sqrt(-2) = %g+i%g \n exp(i) = %g + i%g \n exp(i*pi) = %g + i %g \n i^e = %g + i %g\n ",creal(sqrt),cimag(sqrt),creal(ei),cimag(ei),creal(eipi),cimag(eipi),creal(ie),cimag(ie)) ;
  float a =       0.111111111111111111111111111111111111;
  double b =      0.111111111111111111111111111111111111;
  long double c = 0.111111111111111111111111111111111111L;
  printf("float = %.25g\n double = %.25lg\n long double = %.25Lg\n",a,b,c);
}
  

