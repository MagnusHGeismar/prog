#include<stdio.h>
//#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_eigen.h>

int main(){
/*Eigenvalue Problem*/

/*Creating the Hamiltonian Matrix*/
  int n = 1e2;
  double stepsize = 1.0/(n+1) ;

  gsl_matrix *H = gsl_matrix_calloc(n,n);

  gsl_matrix_set(H,0,0,-2);
  gsl_matrix_set(H,0,1,1);

  gsl_matrix_set(H,n-1,n-1,-2);
  gsl_matrix_set(H,n-1,n-2,1);

  for(int i=1;i< n-1;i++){
    gsl_matrix_set(H,i,i,-2);
    gsl_matrix_set(H,i,i+1,1);
    gsl_matrix_set(H,i,i-1,1);
  }

/*section for printing out the Hamiltonian for troubleshooting*/
//  for (int l=0;l<n;l++){
//    printf("\n");
//    for (int j=0;j<n;j++){
//      printf("%g \t",gsl_matrix_get(H,l,j));
//    }
//  }
//  printf("\n");


  gsl_matrix_scale(H,-pow(stepsize,-2));

/*Allocate workspace for the diagonalisation function, and a vector and a matrix for the results*/

  gsl_eigen_symmv_workspace *eigenval_workspace = gsl_eigen_symmv_alloc(n);

  gsl_vector *eigenvals=gsl_vector_calloc(n);
  gsl_matrix *eigenvecs=gsl_matrix_calloc(n,n);

/*Call the diagonaliser*/

  gsl_eigen_symmv(H,eigenvals,eigenvecs,eigenval_workspace);

/*The eigenvalues are currently arranged in random order,
so we need to sort them in order of ascending values*/
  gsl_eigen_symmv_sort(eigenvals,eigenvecs,GSL_EIGEN_SORT_VAL_ASC);

  fprintf(stderr,"k \t analytical \t calculated\n");
  for (int k=0; k<n/3; k++){
    double ana = pow(M_PI*(k+1),2);
    double calc = gsl_vector_get(eigenvals,k);
    double eps = 0.1;
    if(fabs(ana-calc) > eps){fprintf(stderr,"%i \t %g \t %g\n",k,ana,calc);}
  }

  for(int i=0;i<n;i++){
    printf("\n %g",stepsize*i );
    for(int j=0;j<n;j++){
      printf("%g \t",gsl_matrix_get(eigenvecs,i,j));
    }
  }
  printf("\n");



  gsl_eigen_symmv_free(eigenval_workspace);
  gsl_matrix_free(H);

  return 0;
}
