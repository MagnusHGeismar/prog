#include<stdio.h>
//#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_eigen.h>

int main(){
/*allocates memory for the vectors and matrix, and sets all elements to 0*/
  gsl_vector *v = gsl_vector_calloc(3) ;
  gsl_vector *x = gsl_vector_calloc(3) ;
  gsl_matrix *A = gsl_matrix_calloc(3,3);

/*Setting the values of the elements "set(*object*,*element*,*value*)"*/
  gsl_vector_set(v,0,6.23);
  gsl_vector_set(v,1,5.37);
  gsl_vector_set(v,2,2.29);

  gsl_matrix_set(A,0,0,6.13);
  gsl_matrix_set(A,0,1,-2.90);
  gsl_matrix_set(A,0,2,5.86);

  gsl_matrix_set(A,1,0,8.08);
  gsl_matrix_set(A,1,1,-6.31);
  gsl_matrix_set(A,1,2,-3.89);

  gsl_matrix_set(A,2,0,-4.36);
  gsl_matrix_set(A,2,1,1.00);
  gsl_matrix_set(A,2,2,0.19);

/*Solves the equation Ax=v*/
  gsl_linalg_HH_solve(A,v,x);

  printf("x1 = %g \nx2 = %g\nx3 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1),gsl_vector_get(x,2));

  gsl_vector_free(v);
  gsl_vector_free(x);
  gsl_matrix_free(A);
}
