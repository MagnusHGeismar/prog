#include<math.h>
#include<gsl/gsl_vector.h>

double dfdx(double x, double y){
  return 2*(x-1) + 400*(pow(x,3)-x*y);
}

double dfdy(double x, double y){
  return 200*(y-pow(x,2));
}

double gradf(gsl_vector * v,void * params,gsl_vector * f){
  double x = gsl_vector_get(v,0);
  double y = gsl_vector_get(v,1);

  gsl_vector_set(f,0,dfdx(x,y));
  gsl_vector_set(f,1,dfdy(x,y));

  return GSL_SUCCESS;
}
