#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#define  ALG gsl_multiroot_fsolver_hybrids

int gradf(const gsl_vector * v, void * params, gsl_vector * f);

int main(){

  size_t dim = 2;

  gsl_multiroot_function func;
  func.f = &gradf;
  func.n = dim;
  func.params = NULL;

  gsl_multiroot_fsolver *solver = gsl_multiroot_fsolver_alloc(ALG,dim);

  int iter,status;
  gsl_vector *step = gsl_vector_alloc(dim);
  gsl_vector *root = gsl_vector_alloc(dim);
  double epsabs = 1e-3,epsrel=1e-3;

  /*Solving for the roots of dfdx:*/
  gsl_vector *guess = gsl_vector_alloc(dim);
  gsl_vector_set(guess,0,5.0);
  gsl_vector_set(guess,1,-3.0);
  gsl_multiroot_fsolver_set(solver,&func,guess);

  iter=0;
  do{
    iter++;
    status = gsl_multiroot_fsolver_iterate(solver);

    if(status!=0) {break;printf("solver stuck");}

    step = gsl_multiroot_fsolver_dx(solver);
    root = gsl_multiroot_fsolver_root(solver);
    status = gsl_multiroot_test_delta(step,root,epsabs,epsrel);

  }
  while (status == GSL_CONTINUE && iter < 99);

  if(iter==100) printf("Max iterations reached");

  double xroot = gsl_vector_get(root,0);
  double yroot = gsl_vector_get(root,1);

  double froot = pow(1-xroot,2) + 100 * pow(yroot-xroot*xroot,2);

  printf("root = (%g,%g)\n",xroot,yroot);
  printf("f(root) = %g\n",froot);

  gsl_vector_free(step);
  gsl_vector_free(root);
  gsl_vector_free(guess);
  gsl_multiroot_fsolver_free(solver);
}
