#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#define  ALG gsl_multiroot_fsolver_hybrids

double diffsolve(double eps,double r);

int master(const gsl_vector * v, void * params, gsl_vector * f){
    double r = *(double *) params;
    double eps = gsl_vector_get(v,0);

/*Lines 17-18 are used to apply the boundary condition f = r*exp(-kr).
Line 21 is used for the boundary condition f = 0.*/
    double k = sqrt(-2*eps);
    gsl_vector_set(f,0,diffsolve(eps,r)- r*exp(-k*r));  /*- r*exp(-k*r)*/


//    gsl_vector_set(f,0,diffsolve(eps,r));


    return GSL_SUCCESS;

}

int main(int argc, char * argv[]){
  assert(argc==2);
  double rmax = atof(argv[1]);


  size_t dim = 1;

  gsl_multiroot_function func;
  func.f = master;
  func.n = dim;
  func.params = (void *) &rmax;

  gsl_multiroot_fsolver *solver = gsl_multiroot_fsolver_alloc(ALG,dim);

  int iter,status;
  gsl_vector *step = gsl_vector_alloc(dim);
  gsl_vector *root = gsl_vector_alloc(dim);
  double epsabs = 1e-3,epsrel=0;

  /*Solving for the roots of dfdx:*/
  gsl_vector *guess = gsl_vector_alloc(dim);
  gsl_vector_set(guess,0,-1);
  gsl_multiroot_fsolver_set(solver,&func,guess);

  iter=0;
  do{
    iter++;
    status = gsl_multiroot_fsolver_iterate(solver);

    if(status!=0) {break;printf("solver stuck");}

    step = gsl_multiroot_fsolver_dx(solver);
    root = gsl_multiroot_fsolver_root(solver);
    status = gsl_multiroot_test_delta(step,root,epsabs,epsrel);

  }
  while (status == GSL_CONTINUE && iter < 100);

  if(iter==100) printf("Max iterations reached");

  double eps0 = gsl_vector_get(root,0);

  printf("rmax = %g\teps0 = %g\n",rmax,eps0);

/*Make a plot*/

  FILE *plot = fopen("plot.txt","w");
  if(plot == NULL){printf("Error opening plot.txt\n");}

  for (double j = 1e-3; j<rmax;j+=0.1){
    double analytical = j*exp(-j);
    fprintf(plot,"%g\t%g\t%g\n",j,diffsolve(-0.5,j),analytical);
  }
  fclose(plot);

  gsl_vector_free(step);
  gsl_vector_free(root);
  gsl_vector_free(guess);
  gsl_multiroot_fsolver_free(solver);
}
