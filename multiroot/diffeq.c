#include<math.h>
#include<stdio.h>
#include<assert.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

/*This function is essentially the system of linked differential equations*/
int diff_eq(double r, const double y[], double dydt[], void *params){
  double eps = *(double *) params;
  assert(eps<0);

  dydt[0] = y[1];
  dydt[1] = 2.0*(-1.0/r-eps)*y[0];
  return GSL_SUCCESS;

}

double diffsolve(double eps,double r){
  assert(eps<0);
  assert(r>=0);

/*Here we create an object which has the full description of the problem.
The system of equations from above are thrown into the element .fucntion of this struct*/
  gsl_odeiv2_system system;
  system.function = diff_eq ;
  system.jacobian = NULL ;
  system.dimension = 2 ;
  system.params = (void *) &eps ;

/*We now allocate a workspace for the driver to work in*/
  double stepini = 1e-3, abserr = 1e-6, relerr = 1e-6;
  gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(
    &system, gsl_odeiv2_step_rkf45, stepini, abserr, relerr
  );

/*We are now ready to let the driver run. To ensure accuracy we only apply the driver to
a small segment at a time, and put a for-loop around it to repeat.*/
  double rmin = 1e-3;
  double t = rmin;
  double y[]={t-t*t,1.0-2.0*t};

  if(r<rmin) return r-r*r ;

  int report = gsl_odeiv2_driver_apply(driver,&t,r,y);
  if (report != GSL_SUCCESS){fprintf(stderr,"Error in integration, report: %i",report);}

  gsl_odeiv2_driver_free(driver);

  return y[0];
}
