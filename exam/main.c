#include<stdio.h>
#include<math.h>

double sqroot(double x);

int main(){

  double x;
//creating an output which can be used for a plot of sqrt(x):
  for(int i=0; i<100;i++){
      x = (double)i*0.1;
      double y = sqroot(x);
      printf("%g\t%g\t%g\n",x,y,sqrt(x));
  }
}
