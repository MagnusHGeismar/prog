#include<stdio.h>
#include<assert.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

#define ALG gsl_odeiv2_step_rk4

int diffeq(double t, const double y[], double dydt[], void * params){
  dydt[0] = 1.0/(2.0*y[0]);
  return GSL_SUCCESS;
}


double sqroot(double x){
  //checking that the input x is valid:
  assert(x>=0);
  fprintf(stderr,"control input: %g\n",x);

  /*the routine does not work for x=0, since there is no way to get
  x=0 into the range [1:4] with the operations below. We therefore
  type in the result for sqrt(0) manually:*/
  if(x==0){return 0;}

/*We now move x into the range [1:4], and store information about the
operations we used to get it there, so that they can be reversed
at the end of the routine:*/
  int inverse =0, multiply =0;

/*if x is below 1, we take the inverse, which
will always be greater than 1*/
  if(x<1){
    inverse = 1;
    x = 1.0/x;
  }

/*Next we repeatedly divide by four until we get a value less than 4:*/
  if(x>4){
    do{
      x = x/4.0;
      multiply++;
    }while(x>4);
  }
/*x should now be between 1 and four. We check that:*/
  fprintf(stderr,"x = %g\ninverse = %i\nmultiply = %i\n",x,inverse,multiply);
  assert(x<=4);
  assert(x>=1);

/*we are now ready to use x for our differential equation*/
  int dim = 1;
  const double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
  double y[] = {1}, t = 1;

/*From here on we simply follow the GSL manual on ordinary differential
equation solving:*/
  gsl_odeiv2_system f;
  f.function = diffeq;
  f.jacobian = NULL;
  f.dimension = dim;
  f.params = NULL;

  gsl_odeiv2_step * stepper = gsl_odeiv2_step_alloc(ALG,dim);

  gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new(
    &f,ALG,hstart,epsabs,epsrel
  );

  int status = gsl_odeiv2_driver_apply(driver,&t,x,y); //evolves our system from y(t) to y(x)
  if (status != GSL_SUCCESS){fprintf(stderr,"Error in integration, status: %i",status);}
/*Our result is stored in the first element of y:*/
  double result = y[0];

  /*We now redo the original operations, used to get x into the range
  [1:4], so  that we get the squareroot of the original number.
  We here use that if we divided the original number with 4,we need
  to multiply the final squareroot with 2. Inversing the squareroot is the same as
  inversing the number:*/
  if(multiply > 0) result *= pow(2,(double)multiply);
  if(inverse == 1) result = 1/result;

  return result;

  gsl_odeiv2_driver_free(driver);
  gsl_odeiv2_step_free(stepper);
}
