#include<stdio.h>
#include"komplex.h"
#include<math.h>
#include<complex.h>

void komplex_print (char *s, komplex a){
  printf("%s (%g, %g) \n", s, a.re, a.im);
}

void komplex_set (komplex* z, double x, double y){
  (*z).re = x;
  (*z).im = y;
}

komplex komplex_new (double x, double y){
  komplex z = {x, y};
  return z;
}

komplex komplex_add (komplex a, komplex b){
  komplex result = {a.re + b.re , a.im + b.im};
  return result;
}

komplex komplex_sub (komplex a, komplex b){
  komplex result = {a.re - b.re , a.im - b.im};
  return result;
}

int komplex_equal (komplex a, komplex b){
  if ( (a.re == b.re)&(a.im == b.im) ) return 1;
  else return 0;
}

komplex komplex_mul (komplex a, komplex b){
  komplex result = {a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re};
  return result;
}

komplex komplex_div (komplex a, komplex b){
  komplex result = { (a.re*b.re+a.im*b.im)/(pow(b.re,2)+pow(b.im,2)) , (a.im*b.re - a.re*b.im)/(pow(b.re,2)+pow(b.im,2))};
  return result;
}

komplex komplex_conjugate(komplex z){
  komplex conjugate = {z.re , -z.im};
  return conjugate;
}

komplex komplex_abs(komplex z){
  komplex result = {sqrt(pow(z.re,2)+pow(z.im,2)), 0};
  return result;
}

komplex komplex_exp(komplex z){
  komplex result = {exp(z.re) * cos(z.im) , exp(z.re) * sin(z.im)};
  return result;
}

komplex komplex_cos(komplex z){
  komplex result = { 0.5*( exp(z.im) * cos(z.re) + exp(-(z.im)) * cos(z.re)) , 0.5*(exp(z.im)* sin(z.re) - exp(-z.im)*sin(z.re))};
  return result;
}


komplex komplex_sin(komplex z){
  komplex result = {0.5*(exp(z.im)*sin(z.re)+exp(-z.im)*sin(z.im)) , 0.5*( exp(z.im)*cos(z.re)-exp(-z.im)*cos(z.re) ) };
  return result;
}

komplex komplex_sqrt(komplex z){
  double a = z.re;
  double b = z.im;
  double complex x = csqrt(a+b*I);
  komplex result = {creal(x), cimag(x) };
  return result;
}
