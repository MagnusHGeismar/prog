#include<stdio.h>
#include"komplex.h"
#define TINY 1e-6
#include<math.h>

int main(){
  komplex a = {1,2}, b = {3,4};

  printf("\ntesting komplex_add... \n");
  komplex r = komplex_add(a,b);
  komplex R = {4,6};
  komplex_print("a=",a);
  komplex_print("b=",b);
  komplex_print("a+b should = ", R);
  komplex_print("a+b actually = ", r);

  if (komplex_equal(R,r) )
    printf("test 'add' passed :) \n");
  else
    printf("test 'add' failed: debug me, please... \n");

  printf("\ntesting komplex_sub... \n");
  r = komplex_sub(a,b);
  komplex_set(&R,-2,-2);
  komplex_print("a=",a);
  komplex_print("b=",b);
  komplex_print("a+b should = ", R);
  komplex_print("a+b actually = ", r);

  if (komplex_equal(R,r) )
    printf("test 'sub' passed :) \n");
  else
    printf("test 'sub' failed: debug me, please... \n");

  printf("\ntesting komplex_mul... \n");
  r = komplex_mul(a,b);
  komplex_set(&R,2,4);
  komplex_print("a=",a);
  komplex_print("b=",b);
  komplex_print("a+b should = ", R);
  komplex_print("a+b actually = ", r);

  if (komplex_equal(R,r) )
    printf("test 'mul' passed :) \n");
  else
    printf("test 'mul' failed: debug me, please... \n");
  
  printf("\ntesting komplex_div... \n");
  r = komplex_div(a,b);
  komplex_set(&R,2,4);
  komplex_print("a=",a);
  komplex_print("b=",b);
  komplex_print("a+b should = ", R);
  komplex_print("a+b actually = ", r);

  if (komplex_equal(R,r) )
    printf("test 'div' passed :) \n");
  else
    printf("test 'div' failed: debug me, please... \n");

  printf("\ntesting komplex_conjugate... \n");
  r = komplex_conjugate(a);
  komplex_set(&R,1,-2);
  komplex_print("a=",a);
  komplex_print("conjugate of a should be = ", R);
  komplex_print("conjugate of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'div' passed :) \n");
  else
    printf("test 'div' failed: debug me, please... \n");

  printf("\ntesting komplex_abs... \n");
  r = komplex_abs(a);
  komplex_set(&R,sqrt(5.0),0);
  komplex_print("a=",a);
  komplex_print("abs of a should be = ", R);
  komplex_print("abs of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'abs' passed :) \n");
  else
    printf("test 'abs' failed: debug me, please... \n");

  printf("\ntesting komplex_exp... \n");
  r = komplex_exp(a);
  komplex_set(&R,sqrt(5.0),0);
  komplex_print("a=",a);
  komplex_print("exp of a should be = ", R);
  komplex_print("exp of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'exp' passed :) \n");
  else
    printf("test 'exp' failed: debug me, please... \n");

  printf("\ntesting komplex_cos... \n");
  r = komplex_cos(a);
  komplex_set(&R,2.0328,3.0519);
  komplex_print("a=",a);
  komplex_print("cos of a should be = ", R);
  komplex_print("cos of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'cos' passed :) \n");
  else
    printf("test 'cos' failed: debug me, please... \n");

  printf("\ntesting komplex_sin... \n");
  r = komplex_sin(a);
  komplex_set(&R,3.1658,1.9596);
  komplex_print("a=",a);
  komplex_print("sin of a should be = ", R);
  komplex_print("sin of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'sin' passed :) \n");
  else
    printf("test 'sin' failed: debug me, please... \n");

  printf("\ntesting komplex_sqrt... \n");
  r = komplex_sqrt(a);
  komplex_set(&R,3.1658,1.9596);
  komplex_print("a=",a);
  komplex_print("sqrt of a should be = ", R);
  komplex_print("sqrt of a yields = ", r);

  if (komplex_equal(R,r) )
    printf("test 'sqrt' passed :) \n");
  else
    printf("test 'sqrt' failed: debug me, please... \n");
}
